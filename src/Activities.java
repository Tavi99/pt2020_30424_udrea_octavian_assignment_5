import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Activities {

    public static List<MonitoredData> Task_1() {
        String fileName = "Activities.txt";
        List<String> textFileLines = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            textFileLines = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<MonitoredData> monitoredDataArray = textFileLines.stream()
                .map(line -> line.split("\\t+"))
                .map(tokens -> { MonitoredData md = new MonitoredData(
                        LocalDateTime.parse(tokens[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                        LocalDateTime.parse(tokens[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                        tokens[2]);
                    return md; })
                .collect(Collectors.toList());

        try {
            FileWriter myWriter = new FileWriter("Task_1.txt");
            for (MonitoredData activity: monitoredDataArray) {
                myWriter.write(activity.getStartTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                myWriter.write("    ");
                myWriter.write(activity.getEndTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                myWriter.write("    ");
                myWriter.write(activity.getActivity());
                myWriter.write("\n");
            }
            myWriter.close();
            return monitoredDataArray;
        } catch (Exception e) { System.out.println("An error occured during TASK 1!"); }
        return null;
    }

    public static void Task_2(List<MonitoredData> monitoredDataArray) {
        int distinctDays = (int) monitoredDataArray.stream()
                .map(d -> d.getStartTime().getDayOfYear())
                .distinct()
                .count();
        try {
            FileWriter myWriter = new FileWriter("Task_2.txt");
            myWriter.write("The number of distinct days that appear in the monitoring data is: " + distinctDays);
            myWriter.close();
        } catch (Exception e) {
            System.out.println("An error occured during TASK 2!");
        }
    }

    public static Map<String, Integer> Task_3(List<MonitoredData> monitoredDataArray) {
        Map<String, Integer> activityMap = new HashMap<>();
        monitoredDataArray.stream().forEach(s -> activityMap.merge(s.getActivity(), 1, Integer::sum));
        try {
            FileWriter myWriter = new FileWriter("Task_3.txt");
            for (Map.Entry<String, Integer> entry: activityMap.entrySet()) {
                myWriter.write("Activity <" + entry.getKey() + "> has appeared " + entry.getValue() + " times.\n");
            }
            myWriter.close();
            return activityMap;
        } catch (Exception e) {
            System.out.println("An error occured during TASK 3!");
        }
        return null;
    }

    public static Map<Integer, Map<String, Long>> Task_4(List<MonitoredData> monitoredDataArray) {
        Map<Integer, Map<String, Long>> activityFrequencyMap =
                monitoredDataArray.stream().collect(Collectors.groupingBy(activity -> activity.getStartTime().getDayOfYear(), Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
        try {
            FileWriter myWriter = new FileWriter("Task_4.txt");
            for (Map.Entry<Integer, Map<String, Long>> entry: activityFrequencyMap.entrySet()) {
                myWriter.write("Day <" + entry.getKey() + "> " + entry.getValue() + "\n");
            }
            myWriter.close();
            return activityFrequencyMap;
        } catch (Exception e) {
            System.out.println("An error occured during TASK 4!");
        }
        return null;
    }

    public static Map<String, Integer> Task_5(List<MonitoredData> monitoredDataArray) {
        Map<String, Integer> activityTimeMap = new HashMap<>();
        monitoredDataArray.stream().forEach(s -> activityTimeMap.merge(s.getActivity(), s.computeTotalDurationOfActivity(), Integer::sum));
        try {
            FileWriter myWriter = new FileWriter("Task_5.txt");
            for (Map.Entry<String, Integer> entry: activityTimeMap.entrySet()) {
                myWriter.write("Activity: " + entry.getKey() + " -> the entire duration over the monitoring period: " + entry.getValue() / 60 + " minutes.\n");
            }
            myWriter.close();
            return activityTimeMap;
        } catch (Exception e) {
            System.out.println("An error occured during TASK 5!");
        }
        return null;
    }

    public static void main(String[] args) {

        List<MonitoredData> monitoredDataArray = Activities.Task_1();
        Activities.Task_2(monitoredDataArray);
        Activities.Task_3(monitoredDataArray);
        Activities.Task_4(monitoredDataArray);
        Activities.Task_5(monitoredDataArray);
    }
}
